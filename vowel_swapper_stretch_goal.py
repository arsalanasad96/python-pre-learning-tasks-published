def vowel_swapper(string):
    # ==============
    string = string.replace("a","4")
    string = string.replace("A", "4")
    string = string.replace("e", "3")
    string = string.replace("E", "3")
    string = string.replace("i", "!")
    string = string.replace("o", "ooo")
    string = string.replace("u", "|_|")
    return string
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
