def factors(number):
    # ==============
    l = []
    for i in range(2, number):
        if(number%i is 0):
            l.append(i)


    return l
    
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
